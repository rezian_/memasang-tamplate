<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataTablesController extends Controller
{
    function datatables (){
        return view('data.data-tables');
    }
}
